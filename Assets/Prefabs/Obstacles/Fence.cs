﻿
using TMPro;
using UnityEngine;

public class Fence : MonoBehaviour
{
    private AutoMovementWorld _worldAutoMovement;
    #pragma warning disable 0649
    [SerializeField] private int _fenceStrenght;
    [SerializeField] private TextMeshProUGUI zombieCounter;
    [SerializeField]private GameObject _smokeScreen;
    [SerializeField] private AudioClip _fenceRattle;
    [SerializeField] private AudioClip _fenceFall;
#pragma warning restore 0649
    private Animator _anim;
    private CameraHandler _cameraHandler;
    private bool _broken;
    private Zombie_Animator[] _zombieAnimators;
    private GameManager _gameManager;
    private float _counter;
    private bool _engaged;
    private float _t = 0;
    
    private void Start()
    {
        _anim = GetComponent<Animator>();
        _cameraHandler = FindObjectOfType<CameraHandler>();
        _worldAutoMovement = FindObjectOfType<AutoMovementWorld>();
        _zombieAnimators = new Zombie_Animator[0];
        _gameManager = FindObjectOfType<GameManager>();
        zombieCounter.text = _fenceStrenght.ToString();
    }

    private void Update()
    {
        if (_engaged)
        {
            _t += Time.deltaTime;
            _counter = Mathf.Lerp(_fenceStrenght, _fenceStrenght - _cameraHandler.zombies, _t / 3f);
            updateUI((int)_counter);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Converted")&&!_broken)
        {
            _zombieAnimators = FindObjectsOfType<Zombie_Animator>();
            foreach (var Zombie_Animator in _zombieAnimators)
            {
                Zombie_Animator.anim.SetBool("Rattle", true);
            }

            _anim.SetTrigger("Rattle");
            AudioManager.PlayEffect(_fenceRattle);
            _worldAutoMovement.setWorldMovement(false);
            _gameManager.ToggleSliderPause(true);
            _broken = true;
            if (_cameraHandler.zombies>=_fenceStrenght)
            {
               Invoke("DropFence",1.4f);
               Invoke("FenceBroken",3f);
            }
            else
            {
                _gameManager.LostGame();
            }
        }
    }

    private void updateUI(int num)
    {
        zombieCounter.text = num.ToString();  
    }

    private void FenceBroken()
    {
        _worldAutoMovement.setWorldMovement(true);
        AudioManager.PlayEffect(_fenceFall);
        Invoke("smokeFence",0.4f);
        _gameManager.ToggleSliderPause(false);
        
    }

    private void DropFence()
    {
        _anim.SetTrigger("Drop");
        _anim.SetBool("Dropped",true);

        _zombieAnimators = FindObjectsOfType<Zombie_Animator>();
        foreach (var Zombie_Animator in _zombieAnimators)
        {
            Zombie_Animator.anim.SetBool("Rattle", false);
        }
    }

    private void smokeFence()
    {
        _smokeScreen.SetActive(true);
    }

    public void setFenceStr(int num)
    {
        _fenceStrenght = num;
        zombieCounter.text = _fenceStrenght.ToString();
    }
}

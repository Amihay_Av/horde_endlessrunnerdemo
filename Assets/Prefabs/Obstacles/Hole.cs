﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hole : MonoBehaviour
{
    
    private CameraHandler _cameraHandler;
    private HordePool _hordePool;
    private GameManager _gameManager;
    private void Start()
    {
        _cameraHandler = FindObjectOfType<CameraHandler>();
        _hordePool = FindObjectOfType<HordePool>();
        _gameManager = FindObjectOfType<GameManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Converted"))
        {
            if (other.GetComponentInParent<Converter>().fell)
            {
                return;
            }
            _cameraHandler.zombies--;
            if (_cameraHandler.zombies <= 0)
            {
                _gameManager.LostGame();  
            }
            _hordePool.updateUI();
            Rigidbody rb = other.GetComponentInParent<Rigidbody>();
            GameObject item = rb.transform.parent.gameObject;
            item.transform.SetParent(transform);
            rb.useGravity = true;
            other.GetComponentInParent<Converter>().fell = true;
            other.GetComponentInParent<Converter>().DestroyMe();
        }
    }
}

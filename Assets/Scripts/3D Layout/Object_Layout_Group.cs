﻿
using UnityEngine;

public class Object_Layout_Group : MonoBehaviour
{
   public Object_Layout_Slot[] ChildrenListObjects;
   public int itemsInLine;
   [SerializeField] private float Padding = 0;
   [SerializeField] private Vector3 Vector=Vector3.zero;
   private Vector3 ToCenter;
   [SerializeField] private bool Centerize = true;
   private Transform _transform;

   private int childCount = 0;
   public void Start()
   {
      _transform = transform;
      updateList();
   }

   public void updateList()
   {
      ChildrenListObjects = new Object_Layout_Slot[0];
      if (this!=null)
      {
         ChildrenListObjects = GetComponentsInChildren<Object_Layout_Slot>();
      }


      for (int i = 0; i < ChildrenListObjects.Length; i++)
      {
         ChildrenListObjects[i].transform.localPosition = Vector * Padding * (i);
         ChildrenListObjects[i].myPosition = ChildrenListObjects[i].transform.localPosition;
      }
      
      if (Centerize&&ChildrenListObjects.Length>0)
      {
         ToCenter = -(ChildrenListObjects[ChildrenListObjects.Length - 1].transform.localPosition)/2;
         transform.localPosition = ToCenter;
      }

   }
   
   
   private void FixedUpdate()
   {
      if (_transform.childCount != childCount)
      {
         childCount = _transform.childCount;
         updateList();
      }
   }
}

﻿
using UnityEngine;

public class Object_Layout_Line : MonoBehaviour
{
   public Object_Layout_LineSlot[] ChildrenListObjects;
   private int _hordeLines;
   #pragma warning disable 0694
   [SerializeField] private float Padding = 0;
   [SerializeField] private Vector3 Vector=Vector3.zero;
   [SerializeField] private bool Centerize = true;
#pragma warning restore 0694
   private int childCount = 0;
   private Vector3 ToCenter;
   private Transform _transform;

   public void Start()
   {
      _transform = transform;
      ChildrenListObjects = new Object_Layout_LineSlot[0];
      for (int i = 0; i < ChildrenListObjects.Length; i++)
      {
         ChildrenListObjects[i].transform.localPosition = Vector * Padding * (i);
      }
      _hordeLines = ChildrenListObjects.Length;
      updateList();
   }

   public void updateList()
   {
      ChildrenListObjects = GetComponentsInChildren<Object_Layout_LineSlot>();

      if (ChildrenListObjects.Length > _hordeLines)
      {
         ChildrenListObjects[ChildrenListObjects.Length - 1].transform.localPosition = Vector * Padding * (_hordeLines);
         _hordeLines++;
      }
     
      
      if (Centerize&&ChildrenListObjects.Length>0)
      {
         ToCenter = -(ChildrenListObjects[ChildrenListObjects.Length - 1].transform.localPosition)/2;
         transform.localPosition = ToCenter;
      }

   }

   public void FixedUpdate()
   {
      if (_transform.childCount != childCount)
      {
         childCount = _transform.childCount;
         updateList();
      }
   }
}

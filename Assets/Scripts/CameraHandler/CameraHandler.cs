﻿
using Cinemachine;
using UnityEngine;

public class CameraHandler : MonoBehaviour
{
    #pragma warning disable 0649
    [SerializeField] private GameObject _camera;
    [SerializeField] private Transform _pos1;
    [SerializeField] private Transform _pos2;
    [SerializeField] private float wiggle;
    [SerializeField] private CinemachineVirtualCamera cam_Ending;
    [SerializeField] private float maxZombies;
    [SerializeField] private GameObject _fires;
#pragma warning restore 0649
    private HordePool _hordePool;
    public float zombies;
    private bool _gameEnded;
    private GameManager _gameManager;
    private AudioClip _burning;
  

    private void Start()
    {
        _gameManager = FindObjectOfType<GameManager>();
        _hordePool = FindObjectOfType<HordePool>();
        zombies = _hordePool.zombiesInHorde;
        _burning = _gameManager.GetWinSound();
        _camera.transform.SetParent(transform);
        _pos1.SetParent(transform);
        _pos2.SetParent(transform);
    }

    private void Update()
    {
        if (GameManager.GamePaused)
        {
            return;
        }
        
        if (!_gameEnded)
        {
            float zombieLines = zombies / 4;
            _camera.transform.localPosition =
                Vector3.Lerp(_pos1.localPosition, _pos2.localPosition, zombieLines / maxZombies);

            if (_camera.transform.position.z > cam_Ending.transform.position.z)
            {
                cam_Ending.Priority = 11;
                Invoke("setFire",3f);
                _gameEnded = true;
            }
        }
    }

    private void setFire()
    {
        _fires.SetActive(true);
        AudioManager.PlayEffect(_burning);
        Won();
    }


    private void Won()
    {
        _gameManager.SetWonGame();
    }
    public void wiggleCamera()
    {
        wiggleForward();
        Invoke("wiggleBack",0.2f);
        Invoke("wiggleForward",0.4f);

    }

    private void moveDir(float wiggle)
    {
        var position = _camera.transform.position;
        position = new Vector3(position.x+wiggle,position.y,position.z);
        _camera.transform.position = position;
    }

    private void wiggleForward()
    {
        moveDir(wiggle);

    }
    private void wiggleBack()
    {
        moveDir(-2 * wiggle); 
    }

    /*private void handleRotation()
    {
        _camera.transform.localRotation = Quaternion.LookRotation(_pointer.transform.position);
    }*/
}

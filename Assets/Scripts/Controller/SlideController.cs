﻿
using System.Collections.Generic;
using UnityEngine;

public class SlideController : MonoBehaviour
{
    private float _xvalue;
#pragma warning disable 0649
    [SerializeField] private float _scrollSpeed;
    [SerializeField] private GameObject _camera;
#pragma warning restore 0649
    public float _clampScreen;
    private Transform _transform;
    private float PreX;
    private bool _paused;
    private HordePool _hordePool;
    private bool _resetCamera;
    private float t;
    private float T = 2;
    private float _yRotValue;
    private Quaternion _cameraResetRotation;

    private void Awake()
    {
        _transform = transform;
        _hordePool = FindObjectOfType<HordePool>();
    }

    private void FixedUpdate()
    {
        if (GameManager.GamePaused)
        {
            return;
        }
        
        if (!_paused)
        {
            
            if (Input.GetMouseButton(0))
            {
                Vector2 xvalue = TouchController.GetSwipe();
                UpdateValue(xvalue.x * 0.05f);
                UpdateCamera(xvalue.x * 0.05f);
                UpdateZombies(false);
                _resetCamera = false;
            }

            if (Input.GetMouseButtonUp(0))
            {
                _resetCamera = true;
                t = 0;
                _yRotValue = _camera.transform.rotation.y;
            }

            if (_resetCamera)
            {
                if (t <= T)
                {
                    t += Time.fixedDeltaTime;
                    _cameraResetRotation = _camera.transform.rotation;
                    _cameraResetRotation.y = Mathf.Lerp(_yRotValue, 0, t / T);
                    _camera.transform.rotation = _cameraResetRotation;
                }
                else
                {
                    _resetCamera = false;
                }
            }
        }
        
    }
    public void UpdateValue(float value)
    {
        float _xvalue = Mathf.Clamp(_transform.position.x, _clampScreen*(-1), _clampScreen);
        _xvalue += value*Time.fixedDeltaTime*_scrollSpeed;
        var position = _transform.position;
        position = new Vector3(_xvalue,position.y,position.z);
        _transform.position = position;
    }
    
    public void UpdateCamera(float value)
    {
        float _xvalue = Mathf.Clamp(_transform.position.x, _clampScreen*(-1), _clampScreen);
        _xvalue += value*Time.fixedDeltaTime*_scrollSpeed;
        var rotation = _camera.transform.rotation;
        rotation = Quaternion.Euler(27.1f,_xvalue,rotation.z);
        _camera.transform.rotation = rotation;
    }

    public void UpdateZombies(bool timer)
    {
        List<Zombie_Controller> zombies = _hordePool._ZombieControllers;
        float resetTimer = 0f;
       
        for (int i = zombies.Count-1; i >= 0 ; i--)
        {
            if (timer)
            {
                resetTimer = zombies[i]._timer;
            }
            else
            {
                resetTimer = resetTimer + 0.01f; 
            }
            zombies[i].updatePosition(_transform,resetTimer);
        }
    }
    
    public void toggleSwipePause(bool pause)
    {
        _paused = pause;
    }
    
}

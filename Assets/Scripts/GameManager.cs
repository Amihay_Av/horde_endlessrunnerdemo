﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

[ExecuteInEditMode]
public class GameManager : MonoBehaviour
{
    [Space]
#pragma warning disable 0649   
    [SerializeField] private bool _isPaused;
    [SerializeField] private AudioClip _music;
    [SerializeField] private AudioClip _zombieTurn;
    [SerializeField] private AudioClip _winSound;
    [SerializeField] private AudioClip _hordeLoop;
    [SerializeField] private float peopleRunningSpeed;
    [SerializeField] private float peopleSafeDistance;
    [SerializeField] private GameObject _player;
    [SerializeField] private GameObject _ConvertEffect;
    [SerializeField] private GameObject _worldParent;
    [SerializeField] private SlideController _playerController;
    [SerializeField] private Canvas _gameUiCanvas;
    [SerializeField] private AudioClip[] _scream;
    [SerializeField] private List<Fence> _fences;
    [SerializeField] private int[] _fencesStr;
#pragma warning restore 0649

    public bool WonGame;
    public static bool GamePaused;
  
    private void Start()
    {
        
        Time.timeScale = 0;
        _gameUiCanvas.enabled = false;
        AudioManager.SetBackground(_music);
        
    }

    public void Setup(int[] fences)
    {
        _fencesStr = fences;
        setFencesStr();
        
    }

    public AudioClip GetZombieTurnAudio() => _zombieTurn;
    public bool GetPause() => _isPaused;
    
    public async void StartGame()
    {
        AudioManager.SetHordeLoop(_hordeLoop);
        ResumeGame();
        _gameUiCanvas.enabled = true;
        Time.timeScale = 1;
    }

    public float GetPeopleRunningSpeed() => peopleRunningSpeed;
    public float GetPeopleSafeDistance() => peopleSafeDistance;

    public AudioClip GetWinSound() => _winSound;
    public GameObject GetPlayer() => _player;

    public GameObject GetDeathEffect() => _ConvertEffect;

    public GameObject GetWorldParent() => _worldParent;

    public AudioClip GetScream()
    {
        int rnd = Random.Range(0, _scream.Length);
        return _scream[rnd];
    }

    public void ToggleSliderPause(bool pause)
    {
        _playerController.toggleSwipePause(pause);
    }

    public static void PauseGame()
    {
        GamePaused = true;
    }
    
    public void LostGame()
    {
        setTimeScaleToNull();
    }
    private void setTimeScaleToNull()
    {
        Time.timeScale = 0;
        LoadGameOverScene();
    }

    public void SetWonGame()
    {
        WonGame = true;
        PauseGame();
        Invoke(nameof(LoadGameOverScene),1.5f);
    }

    public static void ResumeGame()
    {
        GamePaused = false;
       
    }
    
    public void LoadGameOverScene()
    {
        SceneManager.LoadSceneAsync(2, LoadSceneMode.Additive).completed += _SetGameOverResults;
    }
    
    private void _SetGameOverResults(AsyncOperation operation)
    {
        if (WonGame)
        {
            EndGameResults._instance.WonGame();
        }
        else
        {
            EndGameResults._instance.LostGame(); 
        }
    }

    private void setFencesStr()
    {
        for (int i = 0; i < _fences.Count; i++)
        {
            _fences[i].setFenceStr(_fencesStr[i]);
        }
    }
    
}

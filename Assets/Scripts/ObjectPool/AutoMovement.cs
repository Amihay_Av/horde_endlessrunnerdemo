﻿
using UnityEngine;

public class AutoMovement : MonoBehaviour
{
    
#pragma warning disable 0649
    [SerializeField] private Animator _humanAnimator;
#pragma warning restore 0649
    private GameManager _gameManager;
    private AudioClip _scream;
    private float _moveSpeed;
    public bool _paused=true;
    private bool _converted=false;
    private Transform _transform;
    private float safeDistance=0;
    private GameObject _player;
    private static readonly int Running = Animator.StringToHash("Running");
    private static readonly int Turn = Animator.StringToHash("Turn");
    public bool Screams;


    private void Awake()
    {
        
        _gameManager = FindObjectOfType<GameManager>();
        _scream = _gameManager.GetScream();
        _moveSpeed = _gameManager.GetPeopleRunningSpeed();
        safeDistance = _gameManager.GetPeopleSafeDistance();
        _player = _gameManager.GetPlayer();
        _transform = transform;
        _converted = false;
    }

    private void Update()
    {
        if (GameManager.GamePaused)
        {
            _humanAnimator.speed = 0;
            return;
        }
        else
        {
            _humanAnimator.speed = 1;
        }

        if (!_paused)
        {

            var position = _transform.position;
            position = new Vector3(position.x, position.y, position.z + Time.deltaTime * _moveSpeed);
            _transform.position = position;
        }
        else if (!_converted)
        {
            if (Vector3.Distance(_transform.position, _player.transform.position) < safeDistance)
            {
                _StartRunning();
            }
        }
    }

    private void _StartRunning()
    {

        _humanAnimator.SetTrigger(Turn);
        if (Screams)
        {
            AudioManager.PlayEffect(_scream);
            Screams = false;
        }

        _humanAnimator.SetBool(Running,true);
    }
    

    public void StopRunning()
    {
        _humanAnimator.SetBool(Running,false);
        _transform.localPosition = Vector3.zero;
        _paused = true;
        _converted = true;
    }
}

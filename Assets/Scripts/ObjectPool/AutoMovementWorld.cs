﻿
using UnityEngine;

public class AutoMovementWorld : MonoBehaviour
{
    
#pragma warning disable 0649
    [SerializeField] private float _moveSpeed;
#pragma warning restore 0649
    private bool _paused;
    private Transform _transform;
    private void Start()
    {
        _transform = transform;
        _paused = true;
    }
    
    private void Update()
    {
        if (_paused)
        {
            var position = _transform.position;
            position = new Vector3(position.x, position.y, position.z - Time.deltaTime * _moveSpeed);
            _transform.position = position;
        }
    }

    public void setWorldMovement(bool pause)
    {
        _paused = pause;
    }
}

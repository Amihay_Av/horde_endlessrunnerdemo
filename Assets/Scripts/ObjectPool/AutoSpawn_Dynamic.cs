﻿
using UnityEngine;

public class AutoSpawn_Dynamic : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField] private SpawnPoint[] _spawnPoints;
    [SerializeField] private float _spawnRate = 5f;
#pragma warning restore 0649
    private float _spawnIncrease = 0f;
    private float _maxSpawnRate;
    private float _spawnDelay;
    private ObjectPool_Dynamic _pool;
    private float _timer = 0f;

    private GameManager _gameManager;
    private void Awake()
    {
        _pool = GetComponent<ObjectPool_Dynamic>();
        _spawnPoints = GetComponentsInChildren<SpawnPoint>();
        _gameManager = FindObjectOfType<GameManager>();
    }

    void Update()
    {
        if (_gameManager.GetPause()) return;
        if (_spawnDelay > 0)
        {
            _spawnDelay -= Time.deltaTime;
        }
        else
        {
            _timer -= Time.deltaTime;
            if (_timer < 0f)
            {
                _timer = _spawnRate;
                UseObject();
            }
        }

        if (_spawnRate > _maxSpawnRate)
        {
            _spawnRate -= Time.deltaTime * _spawnIncrease;
        }
    }

    public void UseObject()
    {
        int spawnPointToUse = Random.Range(0, _spawnPoints.Length);
        
        GameObject newObject = _pool.UseObject();
        newObject.transform.rotation = Quaternion.identity;
        newObject.transform.localRotation =
            Quaternion.Euler(0, 0, newObject.transform.localRotation.eulerAngles.z);
        
    }
}

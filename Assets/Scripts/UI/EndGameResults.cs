﻿
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameResults : MonoBehaviour
{
   [SerializeField] private Canvas _lose;
   [SerializeField] private Canvas _win;

   public static EndGameResults _instance;

   private void Awake()
   {
      _instance = this;
   }

   public void LostGame()
   {
      _lose.enabled = true;
      _win.enabled = false;
   }
   
   public void WonGame()
   {
      _lose.enabled = false;
      _win.enabled = true;
   }

   public void LoadMainMenu()
   {
      SceneManager.UnloadSceneAsync(2);
      SceneManager.LoadScene(0);
   }
}

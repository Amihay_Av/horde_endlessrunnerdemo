﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenu : MonoBehaviour
{
    private GameManager _gameManager;
    private int[] _gameFences;
    private void Start()
    {
        _gameFences = FindObjectOfType<SettingsHandler>().GetFences();
        SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive).completed += operation =>
        {
            SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(1));
            FindObjectOfType<Settings>().Setup();
        };
        
    }
    
    public void StartGame()
    {
        _gameManager = FindObjectOfType<GameManager>();
        _gameManager.StartGame();
        _gameManager.Setup(_gameFences);
        SceneManager.UnloadSceneAsync(0);
    }

    public void SetFencesStr(int[] fences)
    {
        _gameFences = fences;
    }
    
}

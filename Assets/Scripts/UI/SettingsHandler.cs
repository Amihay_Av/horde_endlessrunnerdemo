﻿
using TMPro;
using UnityEngine;


public class SettingsHandler : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI[] _amountText;
    [SerializeField] private int[] fencesStr;

    private int amount = 0;
    public void Start()
    {
        for (int i = 0; i < _amountText.Length; i++)
        {
            _amountText[i].text = fencesStr[i].ToString();
        }
    }
    public void Update1stFence(float value)
    {
        amount = (int)value;
        fencesStr[0] = amount;
        _amountText[0].text = amount.ToString();
    }
    
    public void Update2ndFence(float value)
    {
        amount = (int)value;
        fencesStr[1] = amount;
        _amountText[1].text = amount.ToString();
    }
    
    public void Update3rdFence(float value)
    {
        amount = (int)value;
        fencesStr[2] = amount;
        _amountText[2].text = amount.ToString();
    }
    
    public void Update4thFence(float value)
    {
        amount = (int)value;
        fencesStr[3] = amount;
        _amountText[3].text = amount.ToString();
    }
    
    
    public void SaveButton()
    {
        FindObjectOfType<MainMenu>().SetFencesStr(fencesStr);
    }

    public int[] GetFences() => fencesStr;
}

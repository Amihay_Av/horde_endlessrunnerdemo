﻿
using UnityEngine;

public class Converter : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField] private GameObject _unZombie;
    [SerializeField] private GameObject _zombie;
    [SerializeField] private float animationSpeed=1;
    [SerializeField] private Animator _animator;
#pragma warning restore 0649
    private HordePool _hordePool;
    private GameObject _deathEffect;
    public bool converted;
    private GameManager _gameManager;
    public bool fell;

    private AudioClip _zombieTurn;

    private void Start()
    {
        _gameManager = FindObjectOfType<GameManager>();
        _deathEffect = _gameManager.GetDeathEffect();
        _hordePool = FindObjectOfType<HordePool>();

        _zombieTurn = _gameManager.GetZombieTurnAudio();
        if (converted)
        {
            Convert();
        }
    }

    public void Convert()
    {
        GameObject item = Instantiate(_deathEffect,_gameManager.GetWorldParent().transform);
        item.transform.position = _unZombie.transform.position;
        AudioManager.PlayEffect(_zombieTurn);
        GetComponent<AutoMovement>().StopRunning();
        _unZombie.SetActive(false);
        _zombie.SetActive(true);
        _hordePool.AddToList(gameObject.transform.parent.gameObject);
        _animator.speed = animationSpeed;
        converted = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Converted")&&!converted)
        {
            Convert();
            convertVibrate();
        }
    }

    public void DestroyMe()
    {
      Invoke("DestroyIn",1f);  
    }

    private void DestroyIn()
    {
        Destroy(gameObject);
    }

    private void convertVibrate()
    {
        //Handheld.Vibrate();
    }
}

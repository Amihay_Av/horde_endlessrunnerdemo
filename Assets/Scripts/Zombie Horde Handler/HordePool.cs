﻿
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class HordePool : MonoBehaviour
{
   #pragma warning disable 0649
    [SerializeField] private  GameObject[] originalPrefab;
    [SerializeField] private Object_Layout_LineSlot originalLinePrefab;
    [SerializeField] private GameObject _camera;
    [SerializeField] private Text _zombieCounter;
    [SerializeField] private int LineSpots;
    public int zombiesInHorde;
#pragma warning restore 0649
    private Transform _parent;
    private Object_Layout_Group _parent_layout;
    private Transform _deck;
    private SlideController _player;
    private bool addedCamera;
    private CameraHandler _cameraHandler;

    public List<Zombie_Controller> _ZombieControllers;
    
    [FormerlySerializedAs("_objectLayoutGroup")] public Object_Layout_Line _objectLayoutLine;

    private void Awake()
    {
        _cameraHandler = FindObjectOfType<CameraHandler>();
        _deck = transform;
        _objectLayoutLine = FindObjectOfType<Object_Layout_Line>();
        _player = FindObjectOfType<SlideController>();
        _ZombieControllers = new List<Zombie_Controller>();
        ShowList(zombiesInHorde);
    }

    public void ShowList(int numberOfZombies)
        {
            int cnt = _deck.childCount;
            for (int i = cnt - 1; i >= 0; i--)
            {
                int childcnt = _deck.GetChild(i).transform.childCount;
                Transform child = _deck.GetChild(i);
                for (int j = childcnt - 1; j >= 0; j--)
                {
                    int cChildcnt = child.GetChild(j).transform.childCount;
                    for (int k = cChildcnt - 1; k >= 0; k--)
                    {
                        DestroyImmediate(child.GetChild(j).GetChild(k).gameObject);   
                    }
                    DestroyImmediate(_deck.GetChild(i).GetChild(j).gameObject);
                }
                DestroyImmediate(_deck.GetChild(i).gameObject);
            }
            int count = 0;
            int LineSpotsRef = 0;
            while(count<numberOfZombies)
            {
                Object_Layout_LineSlot Line = Instantiate(originalLinePrefab, _deck);
                if (!addedCamera)
                {
                    _camera.transform.SetParent(Line.transform);
                    _camera.transform.localPosition = Vector3.zero;
                    addedCamera = true;
                }
                Line.gameObject.SetActive(true);
                _parent_layout = Line.GetComponentInChildren<Object_Layout_Group>();
                _parent = _parent_layout.transform;
                _ZombieControllers.Add(Line.GetComponent<Zombie_Controller>());
                
                if (LineSpotsRef < LineSpots)
                {
                    LineSpotsRef++;
                }
                else
                {
                    LineSpotsRef--;
                }

                _parent_layout.itemsInLine = LineSpotsRef;
                for (int i = 0; i < _parent_layout.itemsInLine; i++)
                {
                    if (count >= numberOfZombies)
                    {
                        break;
                    }
                    GameObject item = Instantiate(originalPrefab[0], _parent);
                    item.gameObject.SetActive(true);
                    count++;
                }
            }
            GetComponent<Object_Layout_Line>().updateList();
            _objectLayoutLine.updateList();
        }

        public void AddToList(GameObject zombie)
        {
            Object_Layout_LineSlot[] lineSlots = GetComponentsInChildren<Object_Layout_LineSlot>();
            bool added = false;
            int itemsToAddToAnewLine = 0;

            for (int i = 0; i < lineSlots.Length; i++)
            {
                Object_Layout_Slot[] lineCards = lineSlots[i].GetComponentsInChildren<Object_Layout_Slot>();
                _parent_layout = lineSlots[i].GetComponentInChildren<Object_Layout_Group>();
                if (lineCards.Length < _parent_layout.itemsInLine)
                {
                    _parent = _parent_layout.transform;
                    added = true;
                    break;
                }
                else
                {
                    if (LineSpots == _parent_layout.itemsInLine)
                    {
                        itemsToAddToAnewLine = _parent_layout.itemsInLine-1;
                    }
                    else
                    {
                        itemsToAddToAnewLine = LineSpots;
                    }
                }
            }
            if(!added)
            {
                Object_Layout_LineSlot Line = Instantiate(originalLinePrefab, _deck);
                Line.gameObject.SetActive(true);
                _parent_layout = Line.GetComponentInChildren<Object_Layout_Group>();
                _parent_layout.itemsInLine = itemsToAddToAnewLine;
                _parent = _parent_layout.transform;
                _ZombieControllers.Add(Line.GetComponent<Zombie_Controller>());
            }
            GameObject item = zombie;
            item.transform.SetParent(_parent);
            _parent_layout.updateList();
            item.gameObject.SetActive(true);
            _player.UpdateZombies(true);
            _cameraHandler.zombies++;
            updateUI();
            _cameraHandler.wiggleCamera();
           
        }

        public void updateUI()
        {
            _zombieCounter.text = _cameraHandler.zombies.ToString(); 
        }
}

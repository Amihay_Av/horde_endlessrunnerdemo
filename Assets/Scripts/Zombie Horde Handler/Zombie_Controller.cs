﻿
using UnityEngine;

public class Zombie_Controller : MonoBehaviour
{
    private Transform _transform;
    private Vector3 _position;

    private Vector3 _targetPosition;

    public float _timer=0;
    [SerializeField] private float moveTime=1;
   
    private SlideController _player;
    

    private void Awake()
    {
        _transform = transform;
        _position = new Vector3();
        _timer = 1;
        _player = FindObjectOfType<SlideController>();
       
        //_position = ;
    }

    private void FixedUpdate()
    {
        if (_timer < moveTime)
        {
            _timer += Time.fixedDeltaTime;
            _transform.position = Vector3.Lerp(_position, _targetPosition, _timer / moveTime);
        }
    }

    public void updatePosition(Transform _target,float timeReset)
    {
        _timer = timeReset;
        _position = _transform.position;    
        float _xvalue = Mathf.Clamp(_target.position.x, _player._clampScreen*(-1), _player._clampScreen);
        _targetPosition = new Vector3(_xvalue,_transform.position.y,_transform.position.z);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class runningDirection : MonoBehaviour
{
    private Transform _transform;
    private bool _fix=false;
    private AutoMovement _autoMovement;
    private void Start()
    {
        _transform = transform;
        _autoMovement = GetComponentInParent<AutoMovement>();
    }

    private void Update()
    {

        if (_fix)
        {
            _transform.rotation = Quaternion.identity;
        }
    }

    public void FixRotation()
    {
        _fix = true;
    }
    
    public void Unpause()
    {
        _autoMovement._paused = false; 
    }
}
